#!/bin/bash

SECRET="`kubectl get secrets | grep default-token| awk '{print $1}'`"
echo "KUBERNETES CLUSTER CA CERTIFICATE ="
kubectl get secret $SECRET -o jsonpath="{['data']['ca\.crt']}" | base64 -D
