#!/bin/bash

if [ "$#" -lt 1 ]; then
   echo "Usage: `basename $0` <cluster name>"
   exit 1
else
   CLUSTER="$1"
fi

CONTEXT="`kubectl config get-contexts | grep "$CLUSTER" | awk '{if ($1 != "*") {print $1} else {print $2}}'`"
INGRESS_DNS="`kubectl --context="$CONTEXT" get service ingress-nginx-ingress-controller -n gitlab-managed-apps -o jsonpath="{.status.loadBalancer.ingress[0].hostname}" |sed 's/%//g'`"
INGRESS_IP="`nslookup $INGRESS_DNS | awk '/^Address:\ [0-9]*/{printf "%s, ", $2} END{print}' | sed 's/, $//'`"

echo ""
echo "CONTEXT:        $CONTEXT"
echo "INGRESS DNS:    $INGRESS_DNS"
echo "INGRESS IP(S):  $INGRESS_IP"
echo ""
echo "For GitLab Auto DevOps domain config you can use one of:"
echo "`echo $INGRESS_IP | awk -F",\ " '{printf "   %s.nip.io", $1}'`"
echo "`echo $INGRESS_IP | awk -F",\ " '{printf "   %s.nip.io", $2}'`"
echo "`echo $INGRESS_IP | awk -F",\ " '{printf "   %s.nip.io", $3}'`"
echo ""
